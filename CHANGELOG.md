# [v2.4.6](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v2.4.5...v2.4.6) (2023-03-29)

No Changes/additions since [OpenCPI Release v2.4.5](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.5)

# [v2.4.5](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v2.4.4...v2.4.5) (2023-03-16)

Changes/additions since [OpenCPI Release v2.4.4](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.4)

### Enhancements
- **osp**: support dual port data interface mode on e31x. (!69)(445c0482)
- **osp**: achieve higher sample rates 1TX1RX and support 2TX2RX (!69)(445c0482)

# [v2.4.4](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v2.4.3...v2.4.4) (2023-01-22)

No Changes/additions since [OpenCPI Release v2.4.3](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.3)

# [v2.4.3](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v2.4.2...v2.4.3) (2022-10-11)

Changes/additions since [OpenCPI Release v2.4.2](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.2)

### Enhancements
- **osp**: converted `drc_e31x_csts` to use conditional slaves. (!65)(cdd31f57)
- **osp**: added additional configurations for when using the `e31x_mimo_xcvr_scdcd_csts` card. (!65)(cdd31f57)

### Miscellaneous
- **doc,osp**: add `README` file containing notes for running `fsk_modem_app.xml`. (!66)(24718399)

# [v2.4.2](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v2.4.1...v2.4.2) (2022-05-26)

No Changes/additions since [OpenCPI Release v2.4.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.1)

# [v2.4.1](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v2.4.0...v2.4.1) (2022-03-16)

Changes/additions since [OpenCPI Release v2.4.0](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.0)

# [v2.4.0](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v2.3.4...v2.4.0) (2022-01-25)

No Changes/additions since [OpenCPI Release v2.3.4](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.4)

# [v2.3.4](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v2.3.3...v2.3.4) (2021-12-17)

No Changes/additions since [OpenCPI Release v2.3.3](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.3)

# [v2.3.3](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v2.3.2...v2.3.3) (2021-11-30)

No Changes/additions since [OpenCPI Release v2.3.2](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.2)

# [v2.3.2](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v2.3.1...v2.3.2) (2021-11-08)

No Changes/additions since [OpenCPI Release v2.3.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.1)

# [v2.3.1](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v2.3.0...v2.3.1) (2021-10-13)

No Changes/additions since [OpenCPI Release v2.3.0](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.0)

# [v2.3.0](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v2.3.0-rc.1...v2.3.0) (2021-09-07)

Changes/additions since [OpenCPI Release v2.3.0-rc.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.0-rc.1)

### Enhancements
- **osp**: add csts fsk_modem_app reference to e31x. (!63)(7e530002)

### Bug Fixes
- **osp**: removed unused i2c device workers from e31x assemblies. (!63)(7e530002)

# [v2.3.0-rc.1](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v2.2.1...v2.3.0-rc.1) (2021-08-26)

No Changes/additions since [OpenCPI Release v2.2.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.2.1)

# [v2.2.1](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v2.2.0...v2.2.1) (2021-07-22)

Changes/additions since [OpenCPI Release v2.2.0](https://gitlab.com/opencpi/opencpi/-/releases/v2.2.0)

### Bug Fixes
- **doc**: remove obsolete LaTeX version of E31X Getting Started Guide. (!62)(55ddd109)

# [v2.2.0](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v2.1.1...v2.2.0) (2021-07-08)

Changes/additions since [OpenCPI Release v2.1.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.1.1)

### Enhancements
- **app,comp**: remove extra/unsupported apps and workers. (!58)(489d89b1)
- **doc**: convert source format to Sphinx/ReST. (!61)(54933cf8)
- **doc**: revise and reorganize content for easier use with OpenCPI Installation Guide. (!61)(54933cf8)

### Bug Fixes
- **doc**: update to reference primary RCC platform and its dependencies. (!61)(54933cf8)
- **doc**: correct information about factory SD card use. (!61)(54933cf8)
- **doc**: clarify distinction between E3XX and e31x notation. (!61)(54933cf8)
- **doc**: clarify that e31x platform does not support E320 USRP. (!61)(54933cf8)

# [v2.1.1](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v2.1.0...v2.1.1) (2021-05-20)

Changes/additions since [OpenCPI Release v2.1.0](https://gitlab.com/opencpi/opencpi/-/releases/v2.1.0)

### Enhancements
- **comp**: initialize dac to mid-scale of output. (!60)(7e2389be)

# [v2.1.0](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v2.1.0-rc.2...v2.1.0) (2021-03-18)

Changes/additions since [OpenCPI Release v2.1.0-rc.2](https://gitlab.com/opencpi/opencpi/-/releases/v2.1.0-rc.2)

### Enhancements
- **comp,hdl base**: update e31x platform worker to drive usingPPS. (!55)(1857603c)
- **hdl base**: generalize timing target paths. (!56)(d1adba94)

### Bug Fixes
- **comp**: fix bug in drc worker implementation. (!54)(412daac1)
- **hdl base**: fix bug in timing target path. (!56)(d1adba94)

### Miscellaneous
- **app**: add timestamping drc for e31x platform. (!57)(70afc00f)

# [v2.1.0-rc.2](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v2.1.0-rc.1...v2.1.0-rc.2) (2021-03-04)

Changes/additions since [OpenCPI Release v2.1.0-rc.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.1.0-rc.1)

### Enhancements
- **runtime**: enable drc stop_config. (!52)(f450c004)

### Bug Fixes
- **comp**: fix bug in drc worker implementation. (!54)(412daac1)

# [v2.1.0-rc.1](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v2.1.0-beta.1...v2.1.0-rc.1) (2021-02-08)

Changes/additions since [OpenCPI Release v2.1.0-beta.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.1.0-beta.1)

### New Features
- **app**: add timegate test application. (!51)(384640e3)

### Enhancements
- **hdl base**: update the e31x_mimo_xcvr_data_src_qadc_ad9361_sub_data_sink_qdac_ad9361_sub_mode_2_cmos.xdc to properly constrain things for the ADC one clock per sample. (!46)(a27c4ed1)
- **hdl base**: use updated/shared zynq_ps module that enabled the ACP, set SDP width to 64. (!50)(745f7563)
- **osp**: replace xilinx13_4 with xlinx19_2_aarch32. (!51)(384640e3)
- **runtime**: implement digital radio controller (DRC). (!47)(cfe6c7dc)

### Bug Fixes
- **comp**: fix build error for the drc_e31x worker. (!48)(a0d47c64)

# [v2.1.0-beta.1](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v2.0.1...v2.1.0-beta.1) (2020-12-17)

No Changes/additions since [OpenCPI Release v2.0.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.0.1)

# [v2.0.1](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v2.0.0...v2.0.1) (2020-11-19)

No Changes/additions since [OpenCPI Release v2.0.0](https://gitlab.com/opencpi/opencpi/-/releases/v2.0.0)

# [v2.0.0](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v2.0.0-rc.2...v2.0.0) (2020-10-06)

No changes/additions since [OpenCPI Release v2.0.0-rc.2](https://gitlab.com/opencpi/opencpi/-/releases/v2.0.0-rc.2)

# [v2.0.0-rc.2](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v2.0.0-rc.1...v2.0.0-rc.2) (2020-09-22)

Changes/additions since [OpenCPI Release v2.0.0-rc.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.0.0-rc.1)

### Bug Fixes
- **hdl base**: update bsv SyncResetA instantiation in e31x platform worker. (!39)(c78c8b11)

# [v2.0.0-rc.1](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v2.0.0-beta.1...v2.0.0-rc.1) (2020-08-29)

Changes/additions since [OpenCPI Release v2.0.0-beta.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.0.0-beta.1)

### New Features
- **osp**: e31x digital radio controller. (!35)(94e30608)

### Bug Fixes
- **app**: fix fsk_dig_radio_ctrlr app component name. (!36)(f6f7bd8e)
- **hdl base**: update utility libraries to reflect framework changes. (!34)(59d5afb1)

# [v2.0.0-beta.1](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/compare/v1.7.0...v2.0.0-beta.1) (2020-07-27)

Changes/additions since [OpenCPI Release v1.7.0](https://gitlab.com/opencpi/opencpi/-/releases/v1.7.0)

### Miscellaneous
- **devops**: remove testing on CentOS 6. (!33)(7573feda)

# [v1.7.0](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v1.6.2...v1.7.0) (2020-07-09)

No change/additions since [OpenCPI Release v1.7.0-rc.1](https://gitlab.com/opencpi/opencpi/-/releases/v1.7.0-rc.1)

# [v1.7.0-rc.1](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v1.6.2...v1.7.0-rc.1) (2020-07-06)

Changes/additions since [OpenCPI Release v1.6.2](https://gitlab.com/opencpi/opencpi/-/releases/v1.6.2)

### New Features
- **tools**: basic support to automate E3xx SD card generation. (!20)(1eebf99e)

### Enhancements
- **devops**: add CI pipeline for building tests for e31x. (!17)(6419d826)
- **hdl base**: add AD9361 ADC and DAC subdevice workers supporting ADC clock domain and ComplexShortWithMetadata protocol. (!25)(0216513d)
- **hdl base**: platform worker support of framework changes to sdp and zynq primitives. (!24)(dada6178)

### Miscellaneous
- **doc**: remove utilization.inc and configurations.inc from repo. (!27)(95788d34)

# [v1.6.2](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v1.6.1...v1.6.2) (2020-04-05)

No changes/additions since [OpenCPI Release v1.6.1](https://gitlab.com/opencpi/opencpi/-/releases/v1.6.1)

# [v1.6.0](https://gitlab.com/opencpi/osp/ocpi.osp.e3xx/-/compare/v1.5.0...v1.6.0) (2020-02-19)

### Enhancements
- **osp**: Update system.xml to reflect new gpsd-related attributes. (!1)(fd864759)
- **osp**: Update device proxies and applications to be consistent with time_server.hdl property and WTI changes. (!2)(e9ae8049)
- **osp**: Add platform configs and assemblies used for timekeeping/timestamping measurements. (!3)(6a3c6865)

### Bug Fixes
- **tools**: Fix exporting of sd card content and udev rules. (!7)(9f63b70f)
- **tools**: Adjust e31x.exports to allow system.xml to be exported properly. (!9)(5866c995)

### Miscellaneous
- **osp**: **BREAKING CHANGE** Renamed e310 OSP to e3xx. (!6)(4a2d1b32)
- **osp**: **BREAKING CHANGE** Rename e3xx HDL platform to e31x (note ocpidev build commands are affected). (!6)(4a2d1b32)
- **osp**: Merge change made to github version of e310 platform after the gitlab fork. (!14)(c62119e3)

### Documentation
- **osp**: Remove erroneous '_s' from .so artifact filenames in docs. (!10)(bd7f15c1)
- **osp**: Update getting started guide to be consistent with the new from source install approach. (!11)(e210bf38)
