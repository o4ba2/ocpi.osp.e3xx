/*
 * THIS FILE WAS ORIGINALLY GENERATED ON Wed Dec  2 23:10:31 2020 CST
 * BASED ON THE FILE: drc_e31x.xml
 * YOU *ARE* EXPECTED TO EDIT IT
 *
 * This file contains the implementation skeleton for the drc_e31x worker in C++
 */

#include "dual_drc_e31x_csts-worker.hh"

// These are the helper classes for the ad9361 helpers
#include "RadioCtrlrConfiguratorTuneResamp.hh"
#include "RadioCtrlrConfiguratorAD9361.hh"
#include "RadioCtrlrNoOSTuneResamp.hh"


using namespace OCPI::RCC; // for easy access to RCC data types and constants
using namespace Dual_drc_e31x_cstsWorkerTypes;

//This is for the generic drc helper class
#include "OcpiDrcProxyApi.hh" // this must be after the "using namespace" of the types namespace

namespace OD = OCPI::DRC;

enum trx_modes { TRX_MODE_RX, TRX_MODE_TX, TRX_MODE_OFF }; 

class Dual_drc_e31x_cstsWorker : public OD::DrcProxyBase { //virtual public Drc_e31xWorkerBase {

  // ================================================================================================
  // To use the ad9361 DRC helper classes, we need a configurator class that combines the basic ad9361
  // one with the tuneresamp soft tuning
  class E31xConfigurator : public OD::ConfiguratorAD9361, public OD::ConfiguratorTuneResamp {
  public:
    E31xConfigurator()
      : OD::ConfiguratorAD9361(DUAL_DRC_E31X_CSTS_RF_PORTS_RX.data[0], DUAL_DRC_E31X_CSTS_RF_PORTS_RX.data[1],
                               DUAL_DRC_E31X_CSTS_RF_PORTS_TX.data[0], DUAL_DRC_E31X_CSTS_RF_PORTS_TX.data[1]),
        OD::ConfiguratorTuneResamp(ad9361MaxRxSampleMhz(), ad9361MaxTxSampleMhz()) {
    }
    // All concrete Configurator classes must have this clone method for virtual copying.
    OD::Configurator *clone() const { return new E31xConfigurator(*this); }
  protected:
    // The virtual callback to impose all constraints.
    void impose_constraints_single_pass() {
      ConfiguratorAD9361::impose_constraints_single_pass();
      ConfiguratorTuneResamp::impose_constraints_single_pass();
      Configurator::impose_constraints_single_pass();
    }
  } m_configurator;

  // ================================================================================================
  // trampoline between the DRC ad9361 helper classes in the framework library and the slave workers
  // accessible from this worker.  It lets the former call the latter
  struct DoSlave : OD::DeviceCallBack {
    Slaves &m_slaves;
    DoSlave(Slaves &slaves) : m_slaves(slaves) {}

    double tx_frequency_MHz = 0.0; 
    double rx_frequency_MHz = 0.0; 

    bool rx2a_mode = false;
    bool rx2b_mode = false;
    

    trx_modes trxa_mode = TRX_MODE_OFF;
    trx_modes trxb_mode = TRX_MODE_OFF;

    void update_leds()
    {

        m_slaves.e31x_filter.set_LED_TXRX2_TX(trxa_mode == TRX_MODE_TX);
        m_slaves.e31x_filter.set_LED_TXRX2_RX(trxa_mode == TRX_MODE_RX);
        m_slaves.e31x_filter.set_LED_RX2_RX(rx2a_mode);
	
        m_slaves.e31x_filter.set_LED_TXRX1_TX(trxb_mode == TRX_MODE_TX);
        m_slaves.e31x_filter.set_LED_TXRX1_RX(trxb_mode == TRX_MODE_RX);
        m_slaves.e31x_filter.set_LED_RX1_RX(rx2b_mode);

    }

    // Function to update the switches/LEDs for frontend A depending on the
    // current frequency and mode selection.
    void frontend_a_update()
    {
        const bool tx_low_band = tx_frequency_MHz < 2940.0;
        const bool rx_low_band = rx_frequency_MHz < 2600.0;

        if (trxa_mode == TRX_MODE_RX) {
            m_slaves.e31x_filter.set_VCTXRX2_V1(0);
            m_slaves.e31x_filter.set_VCTXRX2_V2(1);

            m_slaves.e31x_filter.set_TX_ENABLE2A(0);
            m_slaves.e31x_filter.set_TX_ENABLE2B(0);

            m_slaves.e31x_filter.set_VCRX2_V1(rx_low_band ? 1 : 0);
            m_slaves.e31x_filter.set_VCRX2_V2(rx_low_band ? 0 : 1);
        } else {
            m_slaves.e31x_filter.set_VCTXRX2_V1(1);
            m_slaves.e31x_filter.set_VCTXRX2_V2(tx_low_band ? 0 : 1);

            if (trxa_mode == TRX_MODE_TX) {
                m_slaves.e31x_filter.set_TX_ENABLE2A(tx_low_band ? 0 : 1);
                m_slaves.e31x_filter.set_TX_ENABLE2B(tx_low_band ? 1 : 0);
            } else {
                m_slaves.e31x_filter.set_TX_ENABLE2A(0);
                m_slaves.e31x_filter.set_TX_ENABLE2B(0);
            }

            m_slaves.e31x_filter.set_VCRX2_V1(rx_low_band ? 0 : 1);
            m_slaves.e31x_filter.set_VCRX2_V2(rx_low_band ? 1 : 0);
        }

        update_leds();
    }

    // Function to update the switches/LEDs for frontend B depending on the
    // current frequency and mode selection.
    void frontend_b_update()
    {
        const bool tx_low_band = tx_frequency_MHz < 2940.0;
        const bool rx_low_band = rx_frequency_MHz < 2600.0;

        if (trxb_mode == TRX_MODE_RX) {
            m_slaves.e31x_filter.set_VCTXRX1_V1(1);
            m_slaves.e31x_filter.set_VCTXRX1_V2(0);

            m_slaves.e31x_filter.set_TX_ENABLE1A(0);
            m_slaves.e31x_filter.set_TX_ENABLE1B(0);

            m_slaves.e31x_filter.set_VCRX1_V1(rx_low_band ? 1 : 0);
            m_slaves.e31x_filter.set_VCRX1_V2(rx_low_band ? 0 : 1);
        } else {
            m_slaves.e31x_filter.set_VCTXRX1_V1(tx_low_band ? 0 : 1);
            m_slaves.e31x_filter.set_VCTXRX1_V2(1);

            if (trxb_mode == TRX_MODE_TX) {
                m_slaves.e31x_filter.set_TX_ENABLE1A(tx_low_band ? 0 : 1);
                m_slaves.e31x_filter.set_TX_ENABLE1B(tx_low_band ? 1 : 0);
            } else {
                m_slaves.e31x_filter.set_TX_ENABLE1A(0);
                m_slaves.e31x_filter.set_TX_ENABLE1B(0);
            }

            m_slaves.e31x_filter.set_VCRX1_V1(rx_low_band ? 0 : 1);
            m_slaves.e31x_filter.set_VCRX1_V2(rx_low_band ? 1 : 0);
        }

        update_leds();
    }


    void get_byte(uint8_t /*id_no*/, uint16_t addr, uint8_t *buf) {
      m_slaves.config.getRawPropertyBytes(addr, buf, 1);
    }
    void set_byte(uint8_t /*id_no*/, uint16_t addr, const uint8_t *buf) {
      m_slaves.config.setRawPropertyBytes(addr, buf, 1);
    }
    void set_reset(uint8_t /*id_no*/, bool on) {
      m_slaves.config.set_force_reset(on ? 1 : 0);
    }
    bool isMixerPresent(bool rx, unsigned stream) {
      //return stream == 0 && rx ? m_slaves.rx_complex_mixer0.isPresent() : false;
      return false;
    }
    OD::config_value_t getDecimation(unsigned /*stream*/) {
      //return m_slaves.rx_cic_dec0.isPresent() ? m_slaves.rx_cic_dec0.get_R() : 1;
      return 1;
    }
    OD::config_value_t getInterpolation(unsigned /*stream*/) {
      //return m_slaves.tx_cic_int0.isPresent() ? m_slaves.tx_cic_int0.get_R() : 1;
      return 1;
    }
    OD::config_value_t getPhaseIncrement(bool rx, unsigned /*stream*/) {
      //return rx ? m_slaves.rx_complex_mixer0.get_phs_inc() : 0;
      return 0;
    }
    void setPhaseIncrement(bool rx, unsigned /*stream*/, int16_t inc) {
      //if (rx)
      //  m_slaves.rx_complex_mixer0.set_phs_inc(inc);
      return;
    }
    void initialConfig(uint8_t /*id_no*/, OD::Ad9361InitConfig &config) {
      OD::ad9361InitialConfig(m_slaves.config, m_slaves.data_sub, config);
    }
    void postConfig(uint8_t /*id_no*/) {
      OD::ad9361PostConfig(m_slaves.config);
    }
    void finalConfig(uint8_t /*id_no*/, OD::Ad9361InitConfig &config) {
      // set drive strength and skew 
      m_slaves.config.set_digital_io_digital_io_ctrl(0xC4);
      OD::ad9361FinalConfig(m_slaves.config, config);
    }
    // both of these apply to both channels on the ad9361
    unsigned getRfInput(unsigned /*device*/, OD::config_value_t tuning_freq_MHz) {

      rx_frequency_MHz = tuning_freq_MHz;  
      frontend_a_update();
      frontend_b_update();

      // select rx filter from filter bank 
      if (rx_frequency_MHz < 450.0) {
          m_slaves.e31x_filter.set_RX1_BANDSEL(0b100);
          m_slaves.e31x_filter.set_RX1B_BANDSEL(0b11); //don't care
          m_slaves.e31x_filter.set_RX1C_BANDSEL(0b10);

          m_slaves.e31x_filter.set_RX2_BANDSEL(0b101);
          m_slaves.e31x_filter.set_RX2B_BANDSEL(0b11); //don't care
          m_slaves.e31x_filter.set_RX2C_BANDSEL(0b01);
      } else if (rx_frequency_MHz < 700.0) {
          m_slaves.e31x_filter.set_RX1_BANDSEL(0b010);
          m_slaves.e31x_filter.set_RX1B_BANDSEL(0b11); //don't care
          m_slaves.e31x_filter.set_RX1C_BANDSEL(0b11);

          m_slaves.e31x_filter.set_RX2_BANDSEL(0b011);
          m_slaves.e31x_filter.set_RX2B_BANDSEL(0b11); //don't care
          m_slaves.e31x_filter.set_RX2C_BANDSEL(0b11);
      } else if (rx_frequency_MHz < 1200.0) {
          m_slaves.e31x_filter.set_RX1_BANDSEL(0b000);
          m_slaves.e31x_filter.set_RX1B_BANDSEL(0b11); //don't care
          m_slaves.e31x_filter.set_RX1C_BANDSEL(0b01);

          m_slaves.e31x_filter.set_RX2_BANDSEL(0b001);
          m_slaves.e31x_filter.set_RX2B_BANDSEL(0b11); //don't care
          m_slaves.e31x_filter.set_RX2C_BANDSEL(0b10);
      } else if (rx_frequency_MHz < 1800.0) {
          m_slaves.e31x_filter.set_RX1_BANDSEL(0b001);
          m_slaves.e31x_filter.set_RX1B_BANDSEL(0b10);
          m_slaves.e31x_filter.set_RX1C_BANDSEL(0b11); //don't care

          m_slaves.e31x_filter.set_RX2_BANDSEL(0b000);
          m_slaves.e31x_filter.set_RX2B_BANDSEL(0b01);
          m_slaves.e31x_filter.set_RX2C_BANDSEL(0b11); //don't care
      } else if (rx_frequency_MHz < 2350.0) {
          m_slaves.e31x_filter.set_RX1_BANDSEL(0b011);
          m_slaves.e31x_filter.set_RX1B_BANDSEL(0b11);
          m_slaves.e31x_filter.set_RX1C_BANDSEL(0b11); //don't care

          m_slaves.e31x_filter.set_RX2_BANDSEL(0b010);
          m_slaves.e31x_filter.set_RX2B_BANDSEL(0b11);
          m_slaves.e31x_filter.set_RX2C_BANDSEL(0b11); //don't care
      } else if (rx_frequency_MHz < 2600.0) {
          m_slaves.e31x_filter.set_RX1_BANDSEL(0b101);
          m_slaves.e31x_filter.set_RX1B_BANDSEL(0b01);
          m_slaves.e31x_filter.set_RX1C_BANDSEL(0b11); //don't care

          m_slaves.e31x_filter.set_RX2_BANDSEL(0b100);
          m_slaves.e31x_filter.set_RX2B_BANDSEL(0b10);
          m_slaves.e31x_filter.set_RX2C_BANDSEL(0b11); //don't care
      } else {
          m_slaves.e31x_filter.set_RX1_BANDSEL(0b111); //don't care
          m_slaves.e31x_filter.set_RX1B_BANDSEL(0b11); //don't care
          m_slaves.e31x_filter.set_RX1C_BANDSEL(0b11); //don't care

          m_slaves.e31x_filter.set_RX2_BANDSEL(0b111); //don't care
          m_slaves.e31x_filter.set_RX2B_BANDSEL(0b11); //don't care
          m_slaves.e31x_filter.set_RX2C_BANDSEL(0b11); //don't care
      }
      // select input port 
      if (rx_frequency_MHz < 1200) {
        return 2;
      } else if (rx_frequency_MHz < 2600) {
        return 1; 
      } else { 
        return 0; 
      }
    }

    unsigned getRfOutput(unsigned /*device*/, OD::config_value_t tuning_freq_MHz) { 
      tx_frequency_MHz = tuning_freq_MHz;
      frontend_a_update();
      frontend_b_update();

      // select tx filter from filter bank  
      if (tx_frequency_MHz < 117.7) { 
         m_slaves.e31x_filter.set_TX_BANDSEL(0b111); // 7
      } else if (tx_frequency_MHz < 178.2) {
          m_slaves.e31x_filter.set_TX_BANDSEL(0b110); // 6
      } else if (tx_frequency_MHz < 284.3) {
          m_slaves.e31x_filter.set_TX_BANDSEL(0b101); // 5
      } else if (tx_frequency_MHz < 453.7) {
          m_slaves.e31x_filter.set_TX_BANDSEL(0b100); // 4
      } else if (tx_frequency_MHz < 723.8) {
          m_slaves.e31x_filter.set_TX_BANDSEL(0b011); // 3
      } else if (tx_frequency_MHz < 1154.9) {
          m_slaves.e31x_filter.set_TX_BANDSEL(0b010); // 2
      } else if (tx_frequency_MHz < 1842.6) {
         m_slaves.e31x_filter.set_TX_BANDSEL(0b001); // 1
      } else if (tx_frequency_MHz < 2940.0) {
         m_slaves.e31x_filter.set_TX_BANDSEL(0b000); // 0
      } else {
         m_slaves.e31x_filter.set_TX_BANDSEL(0b111); // 7 (don't care)
      }
      // select output port 
      if (tx_frequency_MHz <= 2940) {
        return 1;
      } else {
        return 0;
      }
    }

  } m_doSlave;

  OD::RadioCtrlrNoOSTuneResamp m_ctrlr;
  OD::ConfigLockRequest m_requests[DUAL_DRC_E31X_CSTS_MAX_CONFIGURATIONS_P];

public:
  Dual_drc_e31x_cstsWorker()
    : m_doSlave(slaves),
      m_ctrlr(0, "dual_drc_e31x", m_configurator, m_doSlave) {
  }
  // ================================================================================================
  // These methods interface with the helper ad9361 classes etc.
  // ================================================================================================
  RCCResult prepare_config(unsigned config) {
    log(8, "DRC: prepare_config: %u", config);
    auto &conf = m_properties.configurations.data[config];
    auto &req = m_requests[config];
    // So here we basically convert the data structure dictated by the drc spec property to the one
    // defined by the older DRC/ad9361 helper classes
    auto nChannels = conf.channels.length;
    req.m_data_streams.resize(nChannels);
    unsigned nRx = 0, nTx = 0;

    for (unsigned n = 0; n < nChannels; ++n) {
      auto &channel = conf.channels.data[n];
      auto &stream = req.m_data_streams[n];

      // TODO handle multi channel ports based on rf port name
      // hardcoded to txb 
      if (channel.rx) {
         m_doSlave.rx2b_mode = true;
      } else {
         m_doSlave.trxb_mode = TRX_MODE_TX;
      }

      stream.include_data_stream_type(channel.rx ?
                                      OD::data_stream_type_t::RX : OD::data_stream_type_t::TX);
      stream.include_data_stream_ID(channel.rx ?
                                    DUAL_DRC_E31X_CSTS_RF_PORTS_RX.data[nRx] :
                                    DUAL_DRC_E31X_CSTS_RF_PORTS_TX.data[nTx]);
      stream.include_routing_ID((channel.rx ? "RX" : "TX") +
                                std::to_string(channel.rx ? nRx : nTx));

      ++(channel.rx ? nRx : nTx);
      stream.include_tuning_freq_MHz(channel.tuning_freq_MHz, channel.tolerance_tuning_freq_MHz);
      stream.include_bandwidth_3dB_MHz(channel.bandwidth_3dB_MHz, channel.tolerance_bandwidth_3dB_MHz);
      stream.include_sampling_rate_Msps(channel.sampling_rate_Msps, channel.tolerance_sampling_rate_Msps);
      stream.include_samples_are_complex(channel.samples_are_complex);
      stream.include_gain_mode(channel.gain_mode);
      if (stream.get_gain_mode().compare("manual")==0)
        stream.include_gain_dB(channel.gain_dB, channel.tolerance_gain_dB);
    }
    // Ideally we would validate them here, but not now.
    return RCC_OK;
  }
  RCCResult start_config(unsigned config) {
    log(8, "DRC: start_config: %u", config);
    
    try {
      return m_ctrlr.request_config_lock(std::to_string(config), m_requests[config]) ? RCC_OK :
        setError("config lock request was unsuccessful, set OCPI_LOG_LEVEL to 8 "
                 "(or higher) for more info");
    } catch (const char* err) {
      return setError(err);
    } catch (std::string &err) {
      return setError(err.c_str());
    } catch (std::exception &e) {
      return setError("std exception: %s", e.what());
    } catch (...) {
      return setError("UNKNOWN EXCEPTION");
    }
    return RCC_OK;
  }
  RCCResult stop_config(unsigned config) { 
    log(8, "DRC: stop_config: %u", config);
    
    // Unlock the configurations when stopping the DRC.
    m_ctrlr.unlock_all();    
    log(8, "DRC: stop_config -> Issued unlock for all configurations"); 

    return RCC_OK;
  }
  RCCResult release_config(unsigned /*config*/) {
    log(8, "DRC: release_config");
    return m_ctrlr.shutdown() ?
      setError("transceiver shutdown was unsuccessful, set OCPI_LOG_LEVEL to 8 "
               "(or higher) for more info") : RCC_OK;
  }
};


DUAL_DRC_E31X_CSTS_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
DUAL_DRC_E31X_CSTS_END_INFO
