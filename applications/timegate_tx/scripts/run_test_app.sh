# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

# assumes network mode and set OCPI_LIBRARY_PATH 
# preload bitstream to allow time_server to settle 
ocpihdl -d pl:0 load ../../artifacts/ocpi.osp.e3xx.timegate_tx_e31x_base_cnt.hdl.0.e31x.bitz
sleep 30
# reset time_server time_now register
ocpihdl -dpl:0 set time_server time_now "0"
# run application 
ocpirun -v ./timegate_tx.xml
# print out time_server properties 
ocpihdl -dpl:0 get -v 1
# reset bitstream 
ocpihdl -d pl:0 load /run/media/mmcblk0p1/opencpi/artifacts/testbias_e31x_base.bitz 
