set -e
set -o pipefail
# embedded system-specific settings
system_mnt_path=/mnt

# app-specific settings
deps_proj_relpath_assets=assets
deps_proj_relpath_e3xx=bsps/ocpi.osp.e3xx
deps_proj_relpaths=( \
 core platform $deps_proj_relpath_assets assets_ts $deps_proj_relpath_e3xx)
name=data_sink_qdac_test_app
target_dir=target-xilinx13_4
aci_dir_path=$system_mnt_path/$deps_proj_relpath_assets/applications/$name
owd_dir_path=$system_mnt_path/$deps_proj_relpath_e3xx/applications/$name
owd_filename=$name.xml
aci_path=$aci_dir_path/$target_dir/$name
owd_path=$owd_dir_path/$owd_filename

set_library_path() {
  unset OCPI_LIBRARY_PATH
  for relpath in ${deps_proj_relpaths[@]}; do
    TMP=
    [ ! -z "$OCPI_LIBRARY_PATH" ] && TMP=$OCPI_LIBRARY_PATH: # skip first
    export OCPI_LIBRARY_PATH=$TMP$system_mnt_path/$relpath
  done
  echo OCPI_LIBRARY_PATH=$OCPI_LIBRARY_PATH
}

set_current_directory() {
  pushd $aci_dir_path
  echo changing directory to $aci_di_path
}

run() {
  echo OCPI_LIBRARY_PATH=$OCPI_LIBRARY_PATH
  $aci_path -t 10 $owd_path
}

return_to_previous_directory() {
  popd
  echo "changing directory (back) to $PWD"
}

set_library_path
set_current_directory
run
return_to_previous_directory
